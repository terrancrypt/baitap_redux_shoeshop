import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "../redux/constant/shoeConstant";

class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 p-2">
        <div className="card">
          <img
            className="card-img-top"
            src={image}
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">
             $ {price}
            </p>
            <a onClick={()=>this.props.handleAddToCart(this.props.data)} className="btn btn-primary text-white">
              ADD TO CART
            </a>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) =>{
  return {
    handleAddToCart: (item) =>{
      let action = {
        type: ADD_TO_CART,
        payload: item,
      }
      dispatch(action);
    }
  }
}

export default connect (null, mapDispatchToProps)(ItemShoe);
