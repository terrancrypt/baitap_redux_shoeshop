import React, { Component } from 'react'
import Cart from './Cart'
import ListShoe from './ListShoe'

export default class ShoeShop_Component extends Component {
  render() {
    return (
      <div>
        <Cart/>
        <ListShoe/>
      </div>
    )
  }
}
