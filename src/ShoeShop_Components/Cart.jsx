import React, { Component } from "react";
import { connect } from "react-redux";
import {
  CHANGE_QUANTITY,
  DOWN_QUANTITY,
  REMOVE_ITEM,
  UP_QUANTITY,
} from "../redux/constant/shoeConstant";

class Cart extends Component {
  renderCart = () => {
    return this.props.cart.map((item) => {
      return (
        <tbody>
          <tr>
            <td>
              <img src={item.image} style={{ width: 50 }} />
            </td>
            <td>{item.name}</td>
            <td>
              <button
                className="btn btn-danger"
                onClick={() =>
                  this.props.handleChangeQuantity(item.id, DOWN_QUANTITY)
                }
              >
                -
              </button>
              <strong className="mx-3">{item.soLuong}</strong>
              <button
                className="btn btn-success"
                onClick={() =>
                  this.props.handleChangeQuantity(item.id, UP_QUANTITY)
                }
              >
                +
              </button>
            </td>
            <td>{item.price * item.soLuong}</td>
            <td>
              <button onClick={()=> this.props.handleRemoveItem(item.id)} className="btn btn-danger">Delete</button>
            </td>
          </tr>
        </tbody>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table text-center">
          <thead>
            <th>Image</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Action</th>
          </thead>
          {this.renderCart()}
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeQuantity: (id, UpOrDown) => {
      let action = {
        type: CHANGE_QUANTITY,
        id: id,
        payload: UpOrDown,
      };
      dispatch(action);
    },
    handleRemoveItem: (id) => {
      let action = {
        type: REMOVE_ITEM,
        payload: id,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
