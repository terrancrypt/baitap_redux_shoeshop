import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderList = () => {
    return this.props.list.map((item) => {
      return <ItemShoe data={item} />;
    });
  };
  render() {
    return <div className="row">
      {this.renderList()}
      </div>;
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.shoeList,
  };
};

export default connect(mapStateToProps)(ListShoe);
