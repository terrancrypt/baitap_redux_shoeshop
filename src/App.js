import ShoeShop_Component from './ShoeShop_Components/ShoeShop_Component';

function App() {
  return (
    <div className="container">
      <ShoeShop_Component/>
    </div>
  );
}

export default App;
